<?php
  function provision($username, $password) {
    // declare the provisioning JSON
    $json = '
    {
      "timeout": 1,
      "webAppId": "68a1473e-dd15-422c-b7dd-047b4819f986",        
      "voice": {
      "username": "%s",
      "domain": "192.168.4.61",
      "inboundCallingEnabled": true,
      "allowedOutboundDestination": "all",
      "auth": {
        "username": "karan",
        "password": "123",
        "realm": "192.168.4.61"
        }
      },
      "urlSchemeDetails": {
        "host": "192.168.4.61",
        "port": "8443",
        "secure": true
      }
    }';

    // inject the provisioning JSON with the params passed to the function
    $json = sprintf($json, $username, $username, $password);

    // configure the curl options
    $ch = curl_init('http://192.168.4.61:8080/gateway/sessions/session');    
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      
    curl_setopt($ch, CURLOPT_HTTPHEADER, [                
      'Content-Type: application/json',
      'Content-Length: ' . strlen($json)    
    ]);


    // execute HTTP POST & close the connection    
    $response = curl_exec($ch);  
    curl_close($ch);

    // return the response from the Gateway
    return $response;
  }

?>